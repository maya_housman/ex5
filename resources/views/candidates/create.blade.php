@extends('layouts.app')
@section('title','Create candidate')
@section('content')
<body class='text-center'>
            <div class="content">
                <div class="title m-b-md">

                    <h1>Create candidates </h1>
                    <form method = "post" action = "{{action('CandidatesController@store')}}">
                    @csrf <!--אבטחת מידע-->

                    <div>
                        <label for = "name">Candidates name </label>
                        <input type = "text" name = "name">
                    </div>
                    <div>
                        <label for = "email">Candidates email </label>
                        <input type = "text" class= "form control" name = "email">
                    </div>
                    <div>
                        <input type = "submit" class="btn btn-outline-info" name = "submit" value = "Create candidate">
                    </div>

                    </form>
                    </body>
@endsection