@extends('layouts.app')
@section('title','Edit candidate')

@section('content')
<body class='text-center'>
        <h1>Edit candidate</h1>
        <form method = "post" action = "{{action('CandidatesController@update', $candidate -> id)}}">
            @method('PATCH')
            @csrf
            <div>
                <label for = "name">Candidate name</label>
                <input type = "text" name = "name" value = {{$candidate -> name}}>
            </div>
            <div>
                <label for = "name">Candidate email</label>
                <input type = "text" name = "email" value = {{$candidate -> email}}>
            </div>
            <div>
                <input type = "submit" class="btn btn-outline-info" name = "submit" value = "Update candidate">
            </div>
        </form> 
        </body> 
@endsection